#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main()
{
    auto now = time(nullptr);
    const auto ltm = localtime(&now);
    const int N = 2;
    int array[N][N];

    std::cout << "array:" << N << "x" << N << std::endl;

    for (auto i = 0; i < N; i++)
    {
        for (auto j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            if (i + j < 10)
                std::cout << " ";
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;


    static int summa = 0;
    for (auto i = 0; i < N; i++)
    {
        if (i == ltm->tm_mday % N)
        {
            std::cout << "index: " << i << std::endl;

            for (auto j = 0; j < N; j++)
            {

                summa += array[i][j];

            }


            std::cout << "summa: " << summa << " ";

        }
        

    }
}